from PIL import Image, ImageDraw, ImageFont

class ImageText:
    def __init__(self, text):
        self._text = text
        self._typeface = "Arial.ttf"
        self._size = 50
        self._color = (255, 255, 255, 255)
        self._stroke_size = 0
        self._stroke_color = (0, 0, 0, 255)

    def __getattr__(self, key):
        return getattr(self.base, key)

    @property
    def base(self):
        dummy_img = Image.new('RGB', (1, 1))
        fnt = ImageFont.truetype(self._typeface, self._size)
        img_size = ImageDraw.Draw(dummy_img).textsize(self._text, font = fnt, stroke_width = self._stroke_size)

        img = Image.new('RGBA', img_size)
        img_draw = ImageDraw.Draw(img)
        img_draw.text((0, 0), self._text, fill=self._color, stroke_width = self._stroke_size, stroke_fill = self._stroke_color, font = fnt)
        return img

    def textsize(self, size):
        self._size = size
        return self

    def typeface(self, typeface):
        self._typeface = typeface
        return self

    def color(self, color):
        self._color = color
        return self

    def stroke(self, size, color = (0, 0, 0, 255)):
        self._stroke_size = size
        self._stroke_color = color
        return self

    def add_footer(self, length):
        pass
