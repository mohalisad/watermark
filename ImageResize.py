from PIL import Image

class ImageResize:
    def __init__(self, img):
        self.img = img
        self.orig_width = img.size[0]
        self.orig_height = img.size[1]
        self._width = self.orig_width
        self._height = self.orig_height

    def __getattr__(self, key):
        return getattr(self.base, key)

    @property
    def base(self):
        return self.img.resize(self.new_size, Image.ANTIALIAS)

    @property
    def width(self):
        return int(self._width)

    @property
    def height(self):
        return int(self._height)

    @property
    def new_size(self):
        return (self.width, self.height)

    def resize_to_width(self, new_width):
        self._height = new_width * self._height / self._width
        self._width = new_width
        return self

    def resize_to_height(self, new_height):
        self._width = new_height * self._width / self._height
        self._height = new_height
        return self

    def resize_to_fit(self, size):
        max_width, max_height = size
        mul = min(max_height / self._height, max_width / self._width)
        self._width = mul * self._width
        self._height = mul * self._height
        return self

    def resize_to_scale(self, scale):
        self._width *= scale
        self._height *= scale
        return self
