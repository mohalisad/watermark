import math

from ImageResize import ImageResize
from ImageMultiply import ImageMultiply

class ImageWatermark:
    def __init__(self, baseImage):
        self.img = baseImage.copy()

    def __getattr__(self, key):
        if key == 'baseImage':
            raise AttributeError()
        return getattr(self.img, key)

    @property
    def base(self):
        return self.img

    def __add_watermark(self, watermark, location, offset = (0, 0)):
        location = (location[0] + offset[0], location[1] + offset[1])
        self.img.paste(watermark, location, watermark)
        return self

    def fill(self, watermark, watermark_scale = 1):
        watermark = ImageResize(watermark).resize_to_scale(watermark_scale)
        watermark = ImageMultiply(watermark).to_fill_size(self.img.size).base
        return self.__add_watermark(watermark, (0, 0))

    def fit(self, watermark, offset = (0, 0)):
        watermark = ImageResize(watermark).resize_to_fit(self.img.size).base
        return self.__add_watermark(watermark, offset)

    def topleft(self, watermark, offset = (0, 0)):
        return self.__add_watermark(watermark, (0, 0), offset)

    def topright(self, watermark, offset = (0, 0)):
        offset = (-offset[0], offset[1])
        return self.__add_watermark(watermark, (self.img.size[0] - watermark.size[0], 0), offset)

    def bottomleft(self, watermark, offset = (0, 0)):
        offset = (offset[0], -offset[1])
        return self.__add_watermark(watermark, (0, self.img.size[1] - watermark.size[1]), offset)

    def bottomright(self, watermark, offset = (0, 0)):
        offset = (-offset[0], -offset[1])
        return self.__add_watermark(watermark, (self.img.size[0] - watermark.size[0], self.img.size[1] - watermark.size[1]), offset)
