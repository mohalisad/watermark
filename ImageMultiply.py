from PIL import Image
import math


class ImageMultiply:
    def __init__(self, img):
        self.img = img

    def __getattr__(self, key):
        if key == 'img':
            raise AttributeError()
        return getattr(self.img, key)

    @property
    def base(self):
        return self.img

    def to_fill_size(self, size):
        rows = math.ceil(size[0] / self.img.size[0])
        cols = math.ceil(size[1] / self.img.size[1])
        return self.to_number(rows, cols)

    def to_number(self, rows, cols):
        new_image = Image.new('RGBA', (self.img.size[0] * rows, self.img.size[1] * cols))
        for i in range(rows):
            for j in range(cols):
                new_image.paste(self.img, (i * self.img.size[0], j * self.img.size[1]), self.img)
        self.img = new_image
        return self
