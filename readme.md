# ImageResize Guide

You can resize your images using ImageResize.

You can resize by a scale or an absolute width or height.

**Example**

resize width(keeping ratio)

    ImageResize(img).resize_to_width(100)

resize height(keeping ratio)

    ImageResize(img).resize_to_height(100)

resize to a scale(keeping ratio)

    ImageResize(img).resize_to_scale(0.5)

You can scale to fit an specific size

    ImageResize(img).resize_to_fit(0.5)

**Example**

resize to a fit(keeping ratio)

    ImageResize(img).resize_to_fit(100, 100)

You can method chain too =)

**Example**

resize to a fit(keeping ratio)

    ImageResize(img).resize_to_width(100).resize_to_scale(0.5)

# ImageMultiply Guide

You can copy your image multiple time to fit a specific size or 2-d number.

If we have sample image hello_text like this:

![hello_text_2-d](samples/image_multiply_base.png)

**Example**

Multiply 2-d number

The output of the code will be

    ImageMultiply(hello_text).to_number(3, 2)

![hello_text_2-d](samples/image_multiply_example.png)

**Example**

Multiply to fill size

The output of the code will be

    ImageMultiply(hello_text).to_fill_size((310, 210))

![hello_text_fill_size](samples/image_multiply_example2.png)

This image has (333, 270) resolution which is bigger than the (310, 210)

# ImageText Guide

You can convert your string to images

**Example**

    ImageText("hello")

![image_text sample](samples/image_text_example.png)

You can add style like that

    hello_text = ImageText("hello")
    hello_text.textsize(50) #Size in px
    hello_text.typeface("Arial.ttf") #Font can be local or system wide
    hello_text.color((255, 255, 255, 255)) #(Red, Green, Blue, Alpha)
    hello_text.stroke(10, (0, 0, 0, 255)) #(StrokeSize, (Red, Green, Blue, Alpha)) default color is black

You can chain these feature too

**Example**

    ImageText("hello").typeface("Chalkboard.ttc").textsize(100).color((0, 255,255)).stroke(10,(255, 0, 0))

![image_text sample](samples/image_text_example2.png)

# ImageWatermark Guide

You can add watermark in topright, topleft, bottomright, bottomleft, fill and fit mode

**base**

![image_text sample](samples/base.jpg)

**w1**

![image_text sample](samples/w1.png)

**w2**

![image_text sample](samples/w2.png)

**w3**

![image_text sample](samples/w3.png)

**Example**

    ImageWatermark(base).topleft(w1).topright(w2)

![image_text sample](samples/image_watermark_example.jpg)

You can set offset too

    ImageWatermark(base).topleft(w1, (10, 200)).topright(w2, (200, 20))

![image_text sample](samples/image_watermark_example2.jpg)

You can add a fill layer above or below of the other watermark

**Below Example**

    ImageWatermark(base).fill(w3).topleft(w1).topright(w2)

![image_text sample](samples/image_watermark_example3.jpg)

**Above Example**

    ImageWatermark(base).topleft(w1).topright(w2).fill(w3)

![image_text sample](samples/image_watermark_example4.jpg)

**Fit Example**

    ImageWatermark(base).fit(w1)

![image_text sample](samples/image_watermark_example5.jpg)
